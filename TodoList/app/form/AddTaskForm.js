Ext.define("TodoList.form.AddTaskForm", {
    extend: 'Ext.form.Panel', 

    id: 'addTask',
    xtype: 'addTask',

    config: {
        title: 'Add task',
        items: [{
            title: 'New task',
            xtype: 'fieldset',
            items:[{
                xtype: 'textfield',
                name : 'author',
                label: 'Author'
            },
            {
                xtype: 'textfield',
                name : 'title',
                label: 'Title'
            },
            {
                xtype: 'textfield',
                name : 'list',
                label: 'List'
            }]
        },
        {
            xtype: 'button',
            id: 'formsubmit',
            text: 'Add',
            width: '200px'
        }]
    }
});