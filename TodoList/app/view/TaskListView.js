Ext.define("TodoList.view.TaskListView", {
    extend: 'Ext.dataview.List',

    id: 'taskListView',
    xtype: 'taskListView',

    config: {
        title: 'Todo tasks',
        store: "TasksStore",
        grouped: true,
        itemTpl: '{author} - {title} ({done})',
    }
});